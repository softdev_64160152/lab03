/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.arisa.lab3;

/**
 *
 * @author Asus
 */
class OXProgram {

    static boolean checkWin(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer, 0)) {
            return true;
        }
        if (checkRow(table, currentPlayer, 1)) {
            return true;
        }
        if (checkRow(table, currentPlayer, 2)) {
            return true;
        }
        if (checkCol(table, currentPlayer, 0)) {
            return true;
        }
        if (checkCol(table, currentPlayer, 1)) {
            return true;
        }
        if (checkCol(table, currentPlayer, 2)) {
            return true;
        }
        if (checkX1(table, currentPlayer)) {
            return true;
        }
        if (checkX2(table, currentPlayer)) {
            return true;
        }
        if (checkDraw(table)) {
            return true;
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        for (int i = 0; i < 3; i++) {
            if (table[row][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        for (int i = 0; i < 3; i++) {
            if (table[i][col] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1(String[][] table, String currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2(String[][] table, String currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[i][table.length - 1 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw(String[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] == "-") {
                    return false;
                }
            }
        }
        return true;
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.arisa.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Asus
 */
public class OXProgramUnitTest {
    
    public OXProgramUnitTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoplayBY_O_output_false(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinRow2BY_O_output_true(){
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinRow1BY_O_output_true(){
        String[][] table = {{"O","O","O"},{"-","X","-"},{"-","X","-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinRow3BY_O_output_true(){
        String[][] table = {{"-","-","x"},{"-","X","-"},{"O","O","O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinRow1BY_X_output_true(){
        String[][] table = {{"X","X","X"},{"-","O","-"},{"-","O","-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinRow2BY_X_output_true(){
        String[][] table = {{"-","O","-"},{"X","X","X"},{"-","O","-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinRow3BY_X_output_true(){
        String[][] table = {{"-","O","-"},{"-","O","-"},{"X","X","X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinCol1BY_X_output_true(){
        String[][] table = {{"X","-","-"},{"X","O","-"},{"X","-","O"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinCol2BY_X_output_true(){
        String[][] table = {{"O","X","-"},{"-","X","-"},{"O","X","-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinCol3BY_X_output_true(){
        String[][] table = {{"O","-","X"},{"-","-","X"},{"O","-","X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinCol1BY_O_output_true(){
        String[][] table = {{"O","X","-"},{"O","X","-"},{"O","-","-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinCol2BY_O_output_true(){
        String[][] table = {{"X","O","-"},{"-","O","-"},{"X","O","-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinCol3BY_O_output_true(){
        String[][] table = {{"X","-","O"},{"-","-","O"},{"X","-","O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinX1BY_X_output_true(){
        String[][] table = {{"X","-","O"},{"-","X","O"},{"-","-","X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinX1BY_O_output_true(){
        String[][] table = {{"O","-","X"},{"-","O","X"},{"-","-","O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinX2BY_X_output_true(){
        String[][] table = {{"O","-","X"},{"-","X","O"},{"X","-","-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckWinX2BY_O_output_true(){
        String[][] table = {{"X","-","O"},{"-","O","X"},{"O","-","-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
    @Test
    public void testCheckDraw_output_true(){
        String[][] table = {{"O","X","X"},{"X","O","O"},{"X","X","O"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }
}
